
var liaison = {
    recenter : function () {
        return navigator.geolocation.getCurrentPosition(function (pos) {
            return goog_map.panTo(new google.maps.LatLng( pos.coords.latitude,
                                                          pos.coords.longitude));
        });
    },
    setname : function() {
        $.ajax({
            dataType : 'json',
            type : 'POST',
            url : '/prefs/name',
            success : function(x) { console.log(x); },
            failure : function(x) { console.log(x); },
            data : { 'name' : $('#name').val() }
        });
    },
    init : function () {
        return navigator.geolocation ?
            navigator.geolocation.getCurrentPosition(liaison.isuccess, liaison.ierror) :
            null;
        // $('#glinker').event('click',
        //                     function() {
        //                         $('#glink').attr('href',$('#glink').val());
        //                         $('#glink').click();
        //                     });
    },
    isuccess : function (pos) {
        goog_pos = new google.maps.LatLng( pos.coords.latitude,
                                           pos.coords.longitude);
        goog_map = new google.maps.Map(
            $('#canvas')[0], {
                center : goog_pos,
                streetViewControlOptions : {
                    style : 'SMALL',
                    position : 'TOP_CENTER'
                },
                zoom : 15,
                streetViewControl : true,
                overviewMapControl : true,
                panControl : true,
                zoomControl : true,
                scaleControl : true,
                rotateControl : true,
                mapTypeId : google.maps.MapTypeId.HYBRID });
        return true;
    },
    make_marker : function (ujs) {
        var d = new Date(0);
        d.setUTCSeconds(ujs.timestamp);
        var mk = new google.maps.Marker( {
            position : new google.maps.LatLng(ujs.latitude, ujs.longitude),
            shape : { coord : [1, 1, 1, 20, 18, 20, 18, 1], type : 'poly' },
            map : goog_map, animation : google.maps.Animation.DROP, title : ujs.uid });
        var iw = new google.maps.InfoWindow({ content : "Marker" });
        google.maps.event.addListener(mk, 'click', function () {
            goog_map.panTo(new google.maps.LatLng(ujs.latitude, ujs.longitude));
            goog_map.setZoom(17);
            return iw.open(goog_map, mk);
        });
        goog_markers.push(mk);
        return true;
    },
    whereami : function() {
        goog_map.setZoom(20);
        liaison.recenter();
        liaison.loader();
    },
    clearmap : function () {
        return $.each(goog_markers, function (idx, val) {
            val.setMap(null);
            return true;
        });
    },
    bcount : function() {
        $.getJSON('/devel/bcount', function(x) {
            $('#bcount').html('Beacons in the database: ' + x.result);
        });
    },
    linkload : function() {
        $(document).ready(function(){
            $.getJSON('/navlink', function(dat) {
                $('#glink').val("http://l.ivy.io/s/" + dat.result);
            });
            liaison.bcount();
        });
    },
    loader : function (tval) {
        var timeval = tval || 'yesterday';
        $.each(goog_markers, function (idx, val) {
            val.setMap(null);
            return true;
        });
        $.getJSON('/gather', function (dat) {
            return $.each(dat, function (k, v) {
                return liaison.make_marker(v);
            });
        });
    },
    beacon : function () {
        return navigator.geolocation && navigator.geolocation.getCurrentPosition(function (pos) {
            return $.ajax({ type : 'POST', url : '/beacon', data : { position : pos } });
        });
    }
};

$(document).ready(function () {
    $('#canvas').css('height','100%');
    liaison.init();
    setTimeout(liaison.beacon, 30000);
    setTimeout(liaison.loader, 60000);
    $.ajax( { url: "/navlink",
              dataType: "json",
              success: function(lx) {
                  $('#navlink').attr('value',"l.ivy.io/s/" + lx.result)
              }});
});
