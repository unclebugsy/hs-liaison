{-# LANGUAGE OverloadedStrings #-}
module Developer where

import           Control.Monad.Trans  (liftIO)

import           Data.Aeson
import qualified Data.Text            as T
import qualified Data.ByteString.Char8 as B

import           Database.MongoDB     hiding (Array)

import           Application

import Snap.Core
import           Snap.Extras.JSON
import           Snap.Snaplet
import           Snap.Snaplet.Session

import           Types

session_dump :: AppHandler ()
session_dump = do
  (tok,ses) <- with sess $ do
    fx <- sessionToList
    lx <- csrfToken
    return (fx,lx)
 
  logError $ B.pack $ (show tok) ++ " " ++ show ses
  pass

bcount :: AppHandler ()
bcount = do
  ses <- with sess $ getFromSession "context"
  case ses of
    Nothing -> writeJSON $ object [ "result" .= ("no ctx" :: T.Text)]
    Just ctx -> do
      lx <- liftIO $ run $ count (select ["context" =: ctx] "beacons")
      either
        (\_ -> writeJSON $ object ["result" .= ("0" :: T.Text)])
        (\x -> writeJSON $ object ["result" .= (T.pack $ show x)])
        lx
