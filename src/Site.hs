{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}

module Site where

import           Data.ByteString                             (ByteString)

import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Session.Backends.CookieSession
import           Snap.Util.FileServe

import           Application
import           JSIO
import           Pages
import Developer

routes :: [(ByteString, Handler App App ())]
routes =
  [ ("/gather", disable_cache >> gather)

  , ("/prefs/name", set_name)
  , ("/navlink", navlink)
#ifdef DEVELOPMENT
  , ("/", session_dump)
  , ("/devel/bcount", bcount)
#endif

  , ("/beacon", disable_cache >> beacon)
  , ("/accept_disclaimer", disable_cache >> accept_disclaimer)
  , ("/", ifTop $ disable_cache >> ensure_disclaimer)

  , ("/navlink", navlink)

  , ("/s/:context", disable_cache >> set_context)

  , ("/new", create_context)
  , ("/", ifTop $ disable_cache >> gmap)

  , ("", serveDirectory "resources/static") ]


disable_cache :: Handler App App ()
disable_cache = modifyResponse $ addHeader "Cache-Control" "no-cache"

app :: SnapletInit App App
app = makeSnaplet "app" "An snaplet example application." Nothing $ do
  s <- nestSnaplet "sess" sess $
    initCookieSessionManager "site_key.txt" "sess" (Just 3600000)

  addRoutes routes
  return $ App s
