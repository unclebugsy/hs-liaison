{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Types where

import           GHC.Generics

import           Control.Applicative
import           Control.Monad           (liftM)

import           Data.Aeson
import           Data.Bson
import qualified Data.Text               as T
import           Data.UUID.V4

import           Database.MongoDB        hiding (value)
import qualified Database.MongoDB        as M

import           GHC.Generics
import           System.PosixCompat.Time

new_context :: IO T.Text
new_context = liftM (T.take 3) uuid

uuid :: IO T.Text
uuid = do
  lx <- liftM (T.pack . show) nextRandom
  return $ T.concat $ T.splitOn "-" lx

run :: Action IO a -> IO (Either Failure a)
run a = do
  p <- runIOE $ connect (host "127.0.0.1")
  res <- access p master "liaison" a
  return res

class MongoIO a where
  oout :: a -> M.Document
  iin :: M.Document -> Maybe a

data Beacon = Beacon { beacon_creator          :: T.Text
                     , beacon_name             :: T.Text
                     , beacon_context          :: T.Text
                     , beacon_latitude         :: T.Text
                     , beacon_longitude        :: T.Text
                     , beacon_client_timestamp :: T.Text
                     , beacon_real_timestamp   :: T.Text }
              deriving (Generic,Show)

instance FromJSON Beacon
instance ToJSON Beacon where
  toJSON (Beacon c nam con lat lon _ _) =
    object [ "creator" .= c
           , "name" .= nam
           , "context" .= con
           , "latitude" .= lat
           , "longitude" .= lon ]

instance MongoIO Beacon where
  oout (Beacon ui nam ctx lat lon cts ts) =
    [ ("uid" =: ui)
    , ("name" =: nam)
    , ("context" =: ctx)
    , ("latitude" =: lat)
    , ("longitude" =: lon)
    , ("client_timestamp" =: cts)
    , ("timestamp" =: ts) ]

  iin beacon =
    case (,,,,,,) <$> M.look "uid" beacon
         <*> M.look "name" beacon
         <*> M.look "context" beacon
         <*> M.look "latitude" beacon
         <*> M.look "longitude" beacon
         <*> M.look "client_timestamp" beacon
         <*> M.look "timestamp" beacon of
      Nothing -> Nothing
      Just (bid, nam, ctx, lat, lon, cts, ts) ->
        Just $ Beacon
                 (M.typed bid)
                 (M.typed nam)
                 (M.typed ctx)
                 (M.typed lat)
                 (M.typed lon)
                 (M.typed cts)
                 (M.typed ts)

data Log = Log { line :: T.Text }
           deriving (Generic)

instance Show Log
instance MongoIO Log where
  oout (Log l) = [("line" =: l)]
  iin l =
    maybe Nothing (\x -> Just $ Log (typed x)) (M.look "line" l)

epoch :: IO T.Text
epoch = liftM (T.pack . show) epochTime
