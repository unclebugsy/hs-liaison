{-# LANGUAGE TemplateHaskell #-}

module Application where

import Data.Lens.Template
import Snap.Snaplet
import Snap.Snaplet.Session

data App = App {
  _sess :: Snaplet SessionManager
  }

makeLens ''App

type AppHandler = Handler App App
