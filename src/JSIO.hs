{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}

module JSIO where

import           Control.Applicative
import           Control.Monad.Trans  (liftIO)
import           Prelude              hiding (min)

import           Data.Aeson
import qualified Data.Map             as M
import           Data.Maybe           (catMaybes)
import qualified Data.Text            as T
import qualified Data.Text.Encoding   as T

import           Database.MongoDB     hiding (Array)

import           Application

import           Snap.Core
import           Snap.Extras.JSON
import           Snap.Snaplet
import           Snap.Snaplet.Session

import           Types


set_name :: AppHandler ()
set_name = do
  nam <- getParam "name"
  maybe
    (writeJSON $ object [])
    (\name -> do
        _ <- with sess $ do
          setInSession "name" $ T.decodeUtf8 name
          commitSession
        writeJSON $ object ["result" .= ("ok" :: T.Text)]
    )
    nam
      
           
gather :: AppHandler ()
gather = do
  my_context <- with sess $ getFromSession "context"
  Right beacons <- liftIO $
    case my_context of
      Nothing -> return $ Right []
#ifdef DEVELOPMENT
      Just "all" -> run $ find (select [] "beacons") >>= rest
#endif
      Just the_context ->
        run $ find (select [("context" =: the_context)] "beacons") >>= rest

  writeJSON $
    catMaybes $
     (map iin beacons :: [Maybe Beacon])

beacon :: AppHandler ()
beacon = do
  my_context' <- with sess $ getFromSession "context"

  my_crsf <- get_crsf
  name' <- get_name

  let name = maybe "Unknown" id name'

  
  the_params <- getParams
  now <- liftIO epoch
  case (,,,) <$> M.lookup "position[coords][latitude]" the_params
       <*> M.lookup "position[coords][longitude]" the_params
       <*> M.lookup "position[timestamp]" the_params
       <*> my_context' of
    Just ([lat],[lon],[ts],the_context) -> do
      _ <- liftIO $ do
        run $ save "beacons" $ oout $
          Beacon { beacon_creator = my_crsf
                 , beacon_name = name
                 , beacon_context = the_context
                 , beacon_latitude = (T.decodeUtf8 lat)
                 , beacon_longitude = (T.decodeUtf8 lon)
                 , beacon_client_timestamp = (T.decodeUtf8 ts)
                 , beacon_real_timestamp = now }
      writeJSON $ object ["result" .= T.pack "ok"]
    _ -> writeJSON $ object ["result" .= T.pack "error"]
 where
   get_crsf = with sess $ commitSession >> csrfToken
   get_name = with sess $ getFromSession "name"

newest_for_uid :: T.Text -> IO (Maybe Beacon)
newest_for_uid uid = do
  lx <- run $ findOne (select [ "uid" =: uid ] "beacons") { sort = [ "timestamp" =: (1 :: Int) ] }
  either (\_ -> return Nothing)
    (maybe (return Nothing)
     (return . iin))
    lx

dish :: IO [Either Failure [Document]]
dish = do
  Right uids <- run $ distinct "uid" (select [] "beacons")
  let all_uids = map typed uids :: [T.Text]
  mapM (\x ->
         run $ find (select ["uid" =: x] "beacons") { sort = [ "timestamp" =: (1 :: Int) ] } >>= rest
       ) all_uids


